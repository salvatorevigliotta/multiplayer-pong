﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class LoginMenu : MonoBehaviour
{

    public GameObject login_object;
    public GameObject register_object;
    public GameObject loading_object;

    public UnityEngine.UI.InputField input_login_username;
    public UnityEngine.UI.InputField input_login_password;

    public UnityEngine.UI.InputField input_register_username;
    public UnityEngine.UI.InputField input_register_password;
    public UnityEngine.UI.InputField input_register_confirmPassword;
    public UnityEngine.UI.InputField input_register_email;

    public UnityEngine.UI.Text login_error;
    public UnityEngine.UI.Text register_error;
    public UnityEngine.UI.Text database_error;

    //The part of UI currently being shown
    // 0 = login, 1 = register, 2 = logged in, 3 = loading
    private int part = 0;
    private bool isDatabaseUP = true;

    private string username;
    private string password;

    // Use this for initialization
    void Start ()
    {
        part = 3;
        BlankErrors();
        CheckSaveFile();
    }

    private void CheckSaveFile()
    {
        if(File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            UserAccountManager.instance.Load();
        }
        else
        {
            part = 0;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (!isDatabaseUP)
        {
            //TODO: Check database status or Insert Database error message
        }

        //Enables and Disables the defferent objects to show correct part
        if (part == 0)
        {
            login_object.gameObject.SetActive(true);
            register_object.gameObject.SetActive(false);
            loading_object.gameObject.SetActive(false);
        }
        if (part == 1)
        {
            login_object.gameObject.SetActive(false);
            register_object.gameObject.SetActive(true);
            loading_object.gameObject.SetActive(false);
        }
        if (part == 2)
        {
            login_object.gameObject.SetActive(false);
            register_object.gameObject.SetActive(false);
            loading_object.gameObject.SetActive(true);
            // We are logged in - We have already transitioned to a new scene... Hopefully!
            StartLogIN();
        }
        if (part == 3)
        {
            login_object.gameObject.SetActive(false);
            register_object.gameObject.SetActive(false);
            loading_object.gameObject.SetActive(true);
        }
    }

    internal void ShowLogInMenu()
    {
        part = 0;
    }

    private void StartLogIN()
    {
        UserAccountManager.instance.LogIn(username, password);
    }

    void BlankErrors()
    {
        //Blanks all error texts when part is changed e.g. login > Register
        login_error.text = "";
        register_error.text = "";
    }

    public void Login_Register_Button()
    { 
        //Called when the 'Register' button on the login part is pressed
        part = 1; //show register UI
        BlankErrors();
    }

    public void Register_Back_Button()
    { 
        //Called when the 'Back' button on the register part is pressed
        part = 0; //goes back to showing login UI
        BlankErrors();
    }

    public void Data_LogOut_Button()
    { 
        //Called when the 'Log Out' button on the data part is pressed
        part = 0; //goes back to showing login UI

        UserAccountManager.instance.LogOut();

        BlankErrors();
    }

    public void Login_login_Button()
    { 
        //Called when the 'Login' button on the login part is pressed

        if (isDatabaseUP == true)
        {

            //Check fields aren't blank
            if ((input_login_username.text != "") && (input_login_password.text != ""))
            {

                //Check fields don't contain '-' (if they do, login request will return with error and take longer)
                if ((input_login_username.text.Contains("-")) || (input_login_password.text.Contains("-")))
                {
                    //string contains "-" so return error
                    login_error.text = "Unsupported Symbol '-'";
                    input_login_password.text = ""; //blank password field
                }
                else
                {
                    //Ready to send request
                    StartCoroutine(SendLoginRequest(input_login_username.text, input_login_password.text)); //calls function to send login request
                    part = 3; //show 'loading...'
                }

            }
            else
            {
                //One of the fields is blank so return error
                login_error.text = "Field Blank!";
                input_login_password.text = ""; //blank password field
            }

        }

    }

    public void Register_register_Button()
    { //called when the 'Register' button on the register part is pressed

        if (isDatabaseUP == true)
        {

            //check fields aren't blank
            if ((input_register_username.text != "") && (input_register_password.text != "") && (input_register_confirmPassword.text != "") && (input_register_email.text != ""))
            {

                //check username is longer than 4 characters
                if (input_register_username.text.Length > 4)
                {

                    //check password is longer than 6 characters
                    if (input_register_password.text.Length > 6)
                    {

                        //check passwords are the same
                        if (input_register_password.text == input_register_confirmPassword.text)
                        {
                            if (input_register_email.text.Contains("@"))
                            {
                                if ((input_register_username.text.Contains("-")) || (input_register_password.text.Contains("-")) || (input_register_confirmPassword.text.Contains("-")))
                                {

                                    //string contains "-" so return error
                                    register_error.text = "Unsupported Symbol '-'";
                                    input_login_password.text = ""; 
                                    input_register_confirmPassword.text = "";

                                }
                                else
                                {

                                    //ready to send request
                                    StartCoroutine(SendRegisterRequest(input_register_username.text, input_register_password.text, input_register_email.text));
                                    part = 3; //show 'loading...'
                                }
                            }
                            else
                            {
                                //return email not valid
                                register_error.text = "Email not valid!";
                                input_register_password.text = "";
                                input_register_confirmPassword.text = "";
                            }
                        }
                        else
                        {
                            //return passwords don't match error
                            register_error.text = "Passwords don't match!";
                            input_register_password.text = "";
                            input_register_confirmPassword.text = "";
                        }

                    }
                    else
                    {
                        //return password too short error
                        register_error.text = "Password too Short";
                        input_register_password.text = "";
                        input_register_confirmPassword.text = "";
                    }

                }
                else
                {
                    //return username too short error
                    register_error.text = "Username too Short";
                    input_register_password.text = "";
                    input_register_confirmPassword.text = "";
                }

            }
            else
            {
                //one of the fields is blank so return error
                register_error.text = "Field Blank!";
                input_register_password.text = ""; 
                input_register_confirmPassword.text = "";
            }

        }

    }

    private IEnumerator SendLoginRequest(string _username, string _password)
    {
        if (isDatabaseUP)
        {

            WWWForm form = new WWWForm();
            form.AddField("usernamePost", _username);
            form.AddField("passwordPost", _password);

            WWW www = new WWW(DataURL.LoginUserUrl, form);

            yield return www;

            string _result = www.text;

            if (_result == "")
            {
                BlankErrors();
                register_error.text = "Faild to Login!";
            }
            else if (_result == "Success")
            {
                BlankErrors();
                part = 2; //show logged in UI

                input_login_username.text = "";

                username = _username;
                password = _password;
            }
            else
            {
                part = 0; //back to login UI
                login_error.text = _result + " . Try again later.";
            }

            //blank password field
            input_login_password.text = "";
        }
    }

    private IEnumerator SendRegisterRequest(string _username, string _password, string _email)
    {
        if (isDatabaseUP)
        {

            WWWForm form = new WWWForm();
            form.AddField("usernamePost", _username);
            form.AddField("passwordPost", _password);
            form.AddField("emailPost", _email);

            WWW www = new WWW(DataURL.InsertUserUrl, form);

            yield return www;

            string _result = www.text;

            if(_result == "")
            {
                BlankErrors();
                register_error.text = "Faild to register user!";
            }
            else if(_result == "Success")
            {
                BlankErrors();
                part = 2;

                username = _username;
                password = _password;
            }
            else
            {
                part = 1;
                register_error.text = _result + ". Try again later.";
            }

            input_register_password.text = "";
            input_register_confirmPassword.text = "";
        }
    }
}
