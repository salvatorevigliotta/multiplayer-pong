﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UserAccount_Lobby : MonoBehaviour
{

    public GameObject Menu_object;
    public GameObject Loading_object;

    public Text usernameText;
    public const string SceneMenu = "MainMenu";
    public const string SceneAccountMenu = "AccountLobby";
    public const string SceneSinglePlayer = "TestLevel";
    public const string SceneMultiplayer = "MultiplayerLobby";

    void Start()
    {
        if (UserAccountManager.IsLoggedIn)
            usernameText.text = UserAccountManager.LoggedIn_Username;
    }

    public void LogOut()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        if (UserAccountManager.instance)
            UserAccountManager.instance.LogOut();
        else
            SceneManager.LoadScene(SceneMenu);
    }

    public void StartSinglePlayer()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        SceneManager.LoadScene(SceneSinglePlayer);
    }

    public void StartMultiplayer()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        SceneManager.LoadScene(SceneMultiplayer);
    }

    public void Quit()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        Application.Quit();
    }

    public void Back()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        SceneManager.LoadScene(SceneMenu);
    }

    public void Restart()
    {
        Menu_object.SetActive(false);
        Loading_object.SetActive(true);
        SceneManager.LoadScene(SceneSinglePlayer);
    }

}
