﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BallMovement : MonoBehaviour
{

    public float Speed = 15f;
    public float MaxSpeed = 12f;
    public float SpeedMultiplier = 1.25f;

    private Transform tr;
    public Rigidbody2D rb2D;


    void Awake()
    {
        tr = GetComponent<Transform>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start()
    {
        RestartBall();
    }

    // Update is called once per frame
    void Update()
    {
        if(rb2D.velocity.Equals(Vector2.zero))
        {
            KickBall(Vector2.zero);
        }

        if (Mathf.Abs(rb2D.velocity.x) > MaxSpeed)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x / SpeedMultiplier, rb2D.velocity.y);
            Debug.Log(rb2D.velocity + " X ");
        }


        if (Mathf.Abs(rb2D.velocity.y) > MaxSpeed)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, rb2D.velocity.y / SpeedMultiplier);
            Debug.Log(rb2D.velocity + " Y ");
        }

        //TODO : Fixed this if
        if (rb2D.velocity.x == 0)
            rb2D.velocity = new Vector2(Random.Range(-1,1), rb2D.velocity.y);

    }


    void OnCollisionEnter2D(Collision2D other)
    {
        if((other.gameObject.CompareTag("Player")) || (other.gameObject.CompareTag("AI")))
        {
            float y = HitFactor(transform.position, other.transform.position, other.collider.bounds.size.y);
            int temp = 0;
            temp = (other.transform.position.x > 1) ? -1 : 1;
            Vector2 dir = new Vector2(temp, y).normalized;
            KickBall(dir);
        }

        if(other.gameObject.CompareTag("WallTeam0"))
        {
            LevelManager _levelManager = GameObject.FindObjectOfType<LevelManager>();
            if(_levelManager)
            {
                _levelManager.AddScore(1); //Team 1
            }
        }
        if(other.gameObject.CompareTag("WallTeam1"))
        {
            LevelManager _levelManager = GameObject.FindObjectOfType<LevelManager>();
            if (_levelManager)
            {
                _levelManager.AddScore(0); //Team 0
            }
        }
    }

    private void RestartBall()
    {
        rb2D.angularVelocity = 0.0f;
        KickBall(Vector2.zero);
    }

    private void KickBall(Vector2 dir)
    {
        if(dir.Equals(Vector2.zero))
        {
            float random = Random.value;

            Vector3 _direction = (random >= 0.5f) ? new Vector2(random, 1) : new Vector2(random, -1);
            rb2D.AddForce(_direction * Speed);
        }
        else
        {
            Vector2 _velocity = rb2D.velocity;
            rb2D.velocity = dir * _velocity.magnitude * SpeedMultiplier;
        }

    }

    private float HitFactor(Vector2 ballPosition, Vector2 paddlePosition, float paddleHeight)
    {

        return (ballPosition.y - paddlePosition.y) / paddleHeight;
    }

}
