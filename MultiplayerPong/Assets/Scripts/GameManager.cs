﻿using UnityEngine;

public class GameManager : MonoBehaviour
{

    public int ScoreMax = 5;

    public bool isAndroid = false;

    void Awake()
    {
        if(isAndroid)
        {
            Screen.fullScreen = true;
            Screen.orientation = ScreenOrientation.LandscapeLeft;
        }
    }

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void CheckScore(int _score)
    {
        if(_score >= ScoreMax)
        {
            Debug.Log("Game Win");
            //TODO : Add Game Win and Restart Game
        }
    }

    public void CreateUser()
    {

    }


}