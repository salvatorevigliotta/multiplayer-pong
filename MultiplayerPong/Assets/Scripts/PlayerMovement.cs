﻿using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Player))]
public class PlayerMovement : MonoBehaviour
{

    public float speed = 5;
    public float maxPosY = 3;
    public float maxPosX = 9;

    public int team = 0;
    private Transform tr;
    private Rigidbody2D rb2D;
    private Player player;

    void Awake()
    {
        tr = GetComponent<Transform>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start ()
    {
        CheckStartPosition();
    }
	
	// Update is called once per frame
	void Update ()
    {
        float direction;

        if (Input.GetMouseButton(0))
        {
            direction = (Input.mousePosition.x > Screen.width / 2) ? 1 : -1;
        }
        else
        {
            direction = Input.GetAxisRaw("Horizontal");
        }
        
        if(direction != 0)
        {
            CheckMovementBlock(direction);
        }
       
	}

    private void CheckStartPosition()
    {
        if(team == 0)
        {
            tr.position =new Vector3(tr.position.x, -maxPosY, tr.position.z);
        }
        else if(team == 1)
        {
            tr.position = new Vector3(tr.position.x, maxPosY, tr.position.z);
        }
    }

    private void CheckMovementBlock(float dir)
    {
        float nextFramePosX = Mathf.Abs((new Vector2(dir, 0) * speed * Time.deltaTime).x + tr.position.x);

        if (nextFramePosX < maxPosX)
        {
            tr.Translate(new Vector2(dir, 0 ) * speed * Time.deltaTime);
        }
    }
}
