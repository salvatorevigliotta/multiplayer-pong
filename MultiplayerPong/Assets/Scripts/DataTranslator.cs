﻿using UnityEngine;

public class DataTranslator : MonoBehaviour {

    private static string WIN_SYMBOL = "[WIN]";
    private static string LOSE_SYMBOL = "[LOSE]";

    public static string ValuesToData(int _win, int _lose)
    {
        return WIN_SYMBOL + _win + "|" + LOSE_SYMBOL + _lose;
    }

    public static int DataToWin(string _data)
    {
        return int.Parse(DataToValue(_data, WIN_SYMBOL));
    }

    public static int DataToLose(string _data)
    {
        return int.Parse(DataToValue(_data, LOSE_SYMBOL));
    }

    private static string DataToValue(string _data, string _symbol)
    {
        string[] pieces = _data.Split('|');
        foreach (string piece in pieces)
        {
            if (piece.StartsWith(_symbol))
            {
                return piece.Substring(_symbol.Length);
            }
        }

        Debug.LogError(_symbol + " not found in " + _data);
        return "";
    }
}
