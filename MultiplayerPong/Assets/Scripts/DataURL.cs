﻿using UnityEngine;

public class DataURL : MonoBehaviour
{

    public const string InsertUserUrl = "http://salvatorevigliotta.altervista.org/Pong/InsertUser.php";
    public const string LoginUserUrl = "http://salvatorevigliotta.altervista.org/Pong/Login.php";
    public const string GetUserIDUrl = "http://salvatorevigliotta.altervista.org/Pong/GetPlayerID.php";
    public const string GetUserStatsUrl = "http://salvatorevigliotta.altervista.org/Pong/GetUserStats.php";
}
