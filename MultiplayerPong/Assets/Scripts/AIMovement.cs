﻿using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Player))]
public class AIMovement : MonoBehaviour
{

    public float speed = 5;
    public float maxPosY = 3;
    public float maxPosX = 9;

    public int team = 0;
    private Transform tr;
    private Rigidbody2D rb2D;

    private BallMovement ball;
    private Player player;

    void Awake()
    {
        tr = GetComponent<Transform>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    void Start ()
    {
        CheckStartPosition();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!ball)
        {
            GameObject ballMaster = GameObject.FindGameObjectWithTag("Ball");
            if (ballMaster)
                ball = ballMaster.GetComponent<BallMovement>();
        }
            

        CheckMovement();
	}

    private void CheckMovement()
    {
        if (!ball) return;

        if (Mathf.Sign(tr.position.y) == Mathf.Sign(ball.rb2D.velocity.y))
        {
            if (ball.transform.position.x > tr.position.x + 0.410f)
            {
                if (rb2D.velocity.x < 0)
                    rb2D.velocity = Vector2.zero;

                //rb2D.velocity = Vector2.up * speed;
                tr.Translate(Vector2.right * speed * Time.deltaTime);
            }
            else if (ball.transform.position.x < tr.position.x - 0.410f)
            {
                if (rb2D.velocity.x > 0)
                    rb2D.velocity = Vector2.zero;

                //rb2D.velocity = Vector2.down * speed;
                tr.Translate(Vector2.left * speed * Time.deltaTime);
            }
            else
            {
                rb2D.velocity = Vector2.zero;
            }
        }
        else
            rb2D.velocity = Vector2.zero;

    }

    private void CheckStartPosition()
    {
        if (team == 0)
        {
            tr.position = new Vector3(tr.position.x,-maxPosY , tr.position.z);
        }
        else if (team == 1)
        {
            tr.position = new Vector3(tr.position.x, maxPosY, tr.position.z);
        }
    }
}
