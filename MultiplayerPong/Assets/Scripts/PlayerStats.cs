﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStats : MonoBehaviour
{

    public Text WinCount;
    public Text LoseCount;

    private string playerWin;
    private string playerLose;

	// Use this for initialization
	void Start ()
    {
        if(UserAccountManager.IsLoggedIn)
            UserAccountManager.instance.GetPlayerData(OnReceivedData);
	}

    private void OnReceivedData(string _data)
    {
        if(WinCount != null && LoseCount != null)
        {
            WinCount.text = DataTranslator.DataToWin(_data) + " Wins";
            LoseCount.text = DataTranslator.DataToLose(_data) + " Lose";
        }
    }
}
