﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    public int ScoreMax = 5;

    public GameObject PlayerPrefab;
    public GameObject AIPrefab;
    public GameObject BallPrefab;

    

    public float RespawnBallTime = 2.0f;

    private GameObject ai;
    private GameObject player;
    private GameObject ball;

    private bool respawnBallActive = false;

    [SerializeField]
    private Text scoreTextTeam0;
    [SerializeField]
    private Text scoreTextTeam1;
    [SerializeField]
    private Text Win;
    [SerializeField]
    private GameObject endMenu;
    [SerializeField]
    private GameObject LoadingMenu;

    private int scoreTeam0 = 0;
    private int scoreTeam1 = 0;

    public const string SceneMenu = "MainMenu";

    private int scene;

    // Use this for initialization
    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
		if(!player)
        {
            player = Instantiate(PlayerPrefab);
        }
            

        ai = GameObject.FindGameObjectWithTag("AI");
        if (!ai)
        {
            ai = Instantiate(AIPrefab);
        }
        
        ball = GameObject.FindGameObjectWithTag("Ball");

        if(endMenu)
            endMenu.SetActive(false);

        if (LoadingMenu)
            LoadingMenu.SetActive(false);

    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!ball && !respawnBallActive)
        {
            StartCoroutine("InstantiateBall");
        }
	}

    IEnumerator InstantiateBall()
    {
        respawnBallActive = true;
        yield return new WaitForSeconds(RespawnBallTime);

        if (scoreTeam0 < ScoreMax && scoreTeam1 < ScoreMax)
        {
            ball = Instantiate(BallPrefab);
            respawnBallActive = false;
        }

    }

    public void AddScore(int team)
    {
        if(team == 0)
        {
            scoreTeam0 += 1;
            scoreTextTeam0.text = scoreTeam0.ToString();
        }
        else if(team == 1)
        {
            scoreTeam1 += 1;
            scoreTextTeam1.text = scoreTeam1.ToString();
        }
        //GameManager _gameManager = GameObject.FindObjectOfType<GameManager>();
        //if(_gameManager)
        //{
        //    int _score = (scoreTeam0 > scoreTeam1) ? scoreTeam0 : scoreTeam1;
        //    _gameManager.CheckScore(_score);
        //}

        CheckScore();
        StartCoroutine("InstantiateBall");
        Destroy(ball);
    }

    private void CheckScore()
    {
        if(scoreTeam0 >= ScoreMax)
        {
            respawnBallActive = true;
            WinMatch(0);

        }
        if(scoreTeam1 >= ScoreMax)
        {
            respawnBallActive = true;
            WinMatch(1);
        }
    }

    private void WinMatch(int _team)
    {
        StopAllCoroutines();

        if (ball)
        {
            Destroy(ball);
        }

        string teamName = "";
        if(_team == 0)
        {
            teamName = "Enemy";
        }
        if (_team == 1)
            teamName = "Player";

        Win.text = teamName + "Win !!";
        endMenu.SetActive(true);
    }

    public void Back()
    {
        if (LoadingMenu)
            LoadingMenu.SetActive(true);
        if (endMenu)
            endMenu.SetActive(false);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Restart()
    {
        if (LoadingMenu)
            LoadingMenu.SetActive(true);
        if (endMenu)
            endMenu.SetActive(false);

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
