﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UserAccountManager : MonoBehaviour
{

    public static UserAccountManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this);

    }

    public static string LoggedIn_ID { get; protected set; } //stores ID once logged in
    public static string LoggedIn_Username { get; protected set; } //stores username once logged in
    public static string LoggedIn_Win;
    public static string LoggedIn_Lose;
    private static string LoggedIn_Password = ""; //stores password once logged in


    public static bool IsLoggedIn { get; protected set; }

    public string loggedInSceneName = "AccountLobby";
    public string loggedOutSceneName = "MainMenu";

    public delegate void OnDataReceivedCallback(string data);

    internal void LogOut()
    {
        LoggedIn_ID = "";
        LoggedIn_Username = "";
        LoggedIn_Win = "";
        LoggedIn_Lose = "";
        LoggedIn_Password = "";

        IsLoggedIn = false;
        DestroySaveData();
        Debug.Log("User logged out!");

        SceneManager.LoadScene(loggedOutSceneName);
    }

    internal void LogIn(string username, string password)
    {
        //Get Player ID and load AccountLobby
        StartCoroutine(instance.GetIDPlayer(username, password));
    }

    public IEnumerator GetIDPlayer(string _username, string _password)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", _username);

        WWW www = new WWW(DataURL.GetUserIDUrl,form);

        yield return www;

        string _result = www.text;

        if (_result != "")
        {
            LoggedIn_ID = _result;

            LoggedIn_Username = _username;
            LoggedIn_Password = _password;

            IsLoggedIn = true;

            Debug.Log("Logged in as " + _username);
            Save();
            SceneManager.LoadScene(loggedInSceneName);
        }
        else
        {
            LoginMenu loginMenu = FindObjectOfType<LoginMenu>();
            loginMenu.ShowLogInMenu(); 
            loginMenu.register_error.text = "Faild to Login!";
        }

    }

    public void GetPlayerData(OnDataReceivedCallback onDataReceived)
    { 
        if (IsLoggedIn)
        {
            //ready to send request
            StartCoroutine(GetPlayerStats(LoggedIn_ID, onDataReceived)); //calls function to send get data request
        }
    }

    private IEnumerator GetPlayerStats(string _playerID, OnDataReceivedCallback onDataReceived)
    {
        WWWForm form = new WWWForm();
        form.AddField("IDPlayer", _playerID);

        WWW www = new WWW(DataURL.GetUserStatsUrl, form);

        yield return www;

        string _result = www.text;

        if (_result.Contains("ERROR"))
        {
            Debug.Log("Error User Stats");
        }
        else
        {
            if (onDataReceived != null)
                onDataReceived.Invoke(_result);
            LoggedIn_Win = DataTranslator.DataToWin(_result).ToString();
            LoggedIn_Lose = DataTranslator.DataToLose(_result).ToString();
            Save();
        }


    }

    private void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);

        PlayerData data = new PlayerData();
        data.IDPlayerData = LoggedIn_ID;
        data.UsernameData = LoggedIn_Username;
        data.PasswordData = LoggedIn_Password;
        data.WinData = LoggedIn_Win;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            LogIn(data.UsernameData, data.PasswordData);
        }
    }

    public void DestroySaveData()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            File.Delete(Application.persistentDataPath + "/playerInfo.dat");
        }
    }
}

[Serializable]
public class PlayerData
{
    public string UsernameData;
    public string PasswordData;
    public string IDPlayerData;
    public string WinData;
    public string LoseData;
}